const yargs = require('yargs');
//Describing the use of each command
const argv = yargs
    .command('ls', 'lists all projects of todoist')
    .command('activeTask', 'lists all active tasks')
    .command('add', 'adds the new tasks')
    .command('close', 'closes the task')
    .command('delete', 'delete the task')
    .command('update', 'update the task')
    .command('addProject', 'adds the new Project')
    .command('showProject', 'lists particular project in todoist')
    .command('activePtasks', 'lists all active tasks of a project')
    .command('addPtask', 'adds the new tasks in project')
    .command('subTask', 'adds new subtasks in task')
    .command('closePtask', 'closes the task in project')
    .command('deletePtask', 'delete the task in project')
    .command('updatePtask', 'update the task in project')
    .command('fetchTask','lists the relavent tasks')
    .command('changeDueDate','Changes the active tasks duedate')
    .command('deleteATasks','Delete all the active tasks')
    .help()
    .option('find')
    .alias('help', 'h')
    .argv

//Importing all functions
let closedTask = require('./src/closedTask');
let deletedTask = require('./src/deleteTask');
let getActiveProjects = require('./src/getActiveProjects')
let getActiveTasks = require('./src/getActiveTasks')
let addNewTask = require('./src/addNewTask')
let updatedTask = require('./src/updatedTask')
let getActiveTasksInProject=require('./src/getActiveTasksInProject')
let updateTasksInProject=require('./src/updateTaskInProject')
let addTaskInProject=require('./src/addTaskInProject')
let closedTaskInProject=require('./src/closeTaskInProject')
let deletedTaskInProject=require('./src/deleteTaskInProject')
let addNewProject=require('./src/addNewProject')
let getParticularProject=require('./src/getParticularProject');
let addSubTaskInTask=require('./src/addSubtaskInTask')
let fetchTasks=require('./src/fetchReleventTasks')
let changeDueDate=require('./src/changeDueDate')
let deleteAllActiveTasks=require('./src/deleteAllActiveTasks')

//commands for appropriate functions
const cmdControllerMapping={
    ls:getActiveProjects,
    active:getActiveTasks,
    add:addNewTask,
    close:closedTask,
    delete:deletedTask,
    update:updatedTask,
    addProject:addNewProject,
    showProject:getParticularProject,
    activePtasks:getActiveTasksInProject,
    addPtask:addTaskInProject,
    updatePtask:updateTasksInProject,
    closePtask:closedTaskInProject,
    deletePtask: deletedTaskInProject,
    subTask:addSubTaskInTask,
    fetchTask:fetchTasks,
    changeDueDate:changeDueDate,
    deleteAllTasks:deleteAllActiveTasks
}

if(argv._[0] in cmdControllerMapping){
    cmdControllerMapping[argv._[0]]();
}else{
    console.log('\nGiven invalid command \n')
    console.log('Use node fileName --help/--h for relivant commands \n')
}
