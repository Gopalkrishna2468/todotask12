let {fetch,dotenv,TOKEN,readlineSync,readline}=require('../utility')
function closedTask() {
    try {
        var closeDataId = readlineSync.question('Enter the Id:')
        if(closeDataId!==""){
        const r1 = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
       
        r1.question('Are you sure to close the task?', (answer) => {
            var deleted = true;
            if (answer == 'y' || answer == 'Y') {
                let response = fetch(`https://api.todoist.com/rest/v1/tasks/${closeDataId}/close`, {
                    method: "POST",
                    headers: {
                        Authorization: `Bearer ${TOKEN}`,
                    },
                }).then((res)=>{
                    if(res.status!=200)
                    {
                         console.log("Id not valid")
                    }
                    else{
                        console.log('Task Closed');
                    }
                })
                r1.close()
            } else {
                r1.close()
            }
        
        })
    }else{
        console.log('Please enter valid Id')
    }
    } catch (err) {
        console.log(err)
    }

}

module.exports = closedTask;