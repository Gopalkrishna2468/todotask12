
let {fetch,dotenv,TOKEN,readlineSync}=require('../utility')
async function getParticularProject() {

    try {
        const projectId = readlineSync.question('Enter the Id:')
        console.log(projectId);
        const URL=projectId==""?"https://api.todoist.com/rest/v1/projects":`https://api.todoist.com/rest/v1/projects/${projectId}`
        let projectData = await fetch(URL, {
                headers: {
                    Authorization: `Bearer ${TOKEN}`,
                },
            }).then(res => res.json())
            .then(data => {
                return data
            })
        console.table(projectData)
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports=getParticularProject;