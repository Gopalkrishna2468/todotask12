
let {fetch,dotenv,TOKEN} = require('../utility')
async function deleteAllActiveTasks() {
    try {
  
        let Ids = [];
        let taskData = await fetch('https://api.todoist.com/rest/v1/tasks', {
                headers: {
                    Authorization: `Bearer ${TOKEN}`,
                },
            }).then(res => res.json())
            .then(data => {
                return data
            })
        Ids.push(taskData)
        let dat = []
        for (let ids of Ids) {
            for (let i = 0; i < ids.length; i++) {
                dat.push(ids[i].id)

            }
        }
        for (let i = 0; i < taskData.length; i++) {
            let id = parseInt(taskData[i].id)
            let updatedTaskData = await fetch(`https://api.todoist.com/rest/v1/tasks/${id}`, {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${TOKEN}`,
                },
            }).then(()=>{
                console.log("Successfully deleted")
            })
            
        }
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports = deleteAllActiveTasks;