let {fetch,dotenv,TOKEN,readlineSync}=require('../utility')
async function updatedTask() {
    try {
        var updateData = readlineSync.question('Enter the UpdateTaskName:')
        var dataId = readlineSync.question('Enter the TaskId:')
        if(updateData!=="" && dataId!==""){
        let content = {
            "content": `${updateData}`,
            "due_string": "tomorrow at 12:00",
            "due_lang": "en",
            "priority": 4,
        }
        let updatedTaskData = await fetch(`https://api.todoist.com/rest/v1/tasks/${dataId}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${TOKEN}`,
            },
            body: JSON.stringify(content)
        })
        if (updatedTaskData.status == 204) {
            console.log('Task Updated')
        }
    }else{
        console.log('TaskName or TaskId is Invalid')
    }
    } catch (err) {
        console.log(err)
    }
}
module.exports = updatedTask;

