let {fetch,dotenv,TOKEN}=require('../utility')
async function getActiveTasks() {
    try {
        const format=["id","project_id","created","url","description","parent"]
        let taskData = await fetch('https://api.todoist.com/rest/v1/tasks', {
                headers: {
                    Authorization: `Bearer ${TOKEN}`,
                },
            }).then(res => res.json())
            .then(data => {
                return data
            })
            console.table(taskData,format)
        return taskData;
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports = getActiveTasks;



