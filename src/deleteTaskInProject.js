let {fetch,dotenv,TOKEN,readlineSync,readline}=require('../utility')
function deletedTaskInProject() {
    try {

        var deleteData = readlineSync.question('Enter the Id:')
        if(deleteData!==""){
        const r1 = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        r1.question('Are you sure to delete the task?', (answer) => {
            let content = {
                "project_id":2270794857
            }
            var deleted = true;
            if (answer == 'y' || answer == 'Y') {
                let deleteTaskData = fetch(`https://api.todoist.com/rest/v1/tasks/${deleteData}`, {
                    method: "DELETE",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${TOKEN}`,
                    },
                    body: JSON.stringify(content)
                }).then((res)=>{
                    if(res.status!=200)
                    {
                         console.log("Id not valid")
                    }
                    else{
                        console.log('Task Deleted');
                    }
                })
                r1.close()
            } else {
                r1.close()
            }
        })
    }else{
        console.log('Please enter valid Id')
    }
    } catch (err) {
        console.log(err)
    }

}
module.exports=deletedTaskInProject;