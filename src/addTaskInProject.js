let {fetch,dotenv,TOKEN,readlineSync}=require('../utility')
async function addTaskInProject() {
    try {
        var contentData = readlineSync.question('Enter the TaskName:')
        var timeData = readlineSync.question('Enter the TaskTimings:')
        var description = readlineSync.question('Enter the TaskDescription:')
        var projectId=readlineSync.question('Enter the projectId:')
        let content = {
            "content": `${contentData}`,
            "due_string": `${timeData}`,
            "description": `${description}`,
            "priority": 4,
            "project_id":parseInt(projectId)
        }
        let taskData = await fetch('https://api.todoist.com/rest/v1/tasks', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${TOKEN}`,
            },
            body: JSON.stringify(content)
        })
          if (taskData.status == 200) {
                console.log('Task addded Successfully')
            }else {
                console.log("Please enter valid details")
            }
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports=addTaskInProject



