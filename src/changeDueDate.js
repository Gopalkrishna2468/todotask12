let {fetch,dotenv,TOKEN,readlineSync} = require('../utility')
let getActiveTasks=require('./getActiveTasks')
async function changeDueDate() {

    try {
        var dueDate = readlineSync.question('Enter the dueDate for all active tasks:');
        const updatedObject = {
            "due_string": `${dueDate}`,
        };
   
        let taskList=await getActiveTasks();

        const promisesList = [];

        taskList.forEach(task => {
            let promiseObj = fetch("https://api.todoist.com/rest/v1/tasks/" + task.id, {
                method: "POST",
                body: JSON.stringify(updatedObject),
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${TOKEN}`,
                },
            });
           promisesList.push(promiseObj)
        });

       Promise.all(promisesList)
      
       console.log('Duedate changed successfully')
    } catch (err) {
        console.log(err)
    }
}
module.exports = changeDueDate;





