let {fetch,dotenv,TOKEN,readlineSync}=require('../utility')
async function addNewProject() {
    try {
        var projectData = readlineSync.question('Enter the ProjectName:')
        let content = {
            "name": `${projectData}`
        }
        let taskData = await fetch('https://api.todoist.com/rest/v1/projects', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${TOKEN}`,
            },
            body: JSON.stringify(content)
        })
        console.log(taskData)
    } catch {
        (err) => {
            console.log(err)
        }
    }
}
module.exports=addNewProject
