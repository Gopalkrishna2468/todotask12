var config={
    fetch: require('node-fetch'),
    readline : require('readline'),
    readlineSync : require('readline-sync'),
    
    dotenv:require('dotenv').config(),
    TOKEN :process.env.TOKEN
}

module.exports=config;
